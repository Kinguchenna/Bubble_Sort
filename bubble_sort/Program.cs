﻿using System; 
 using System.Collections.Generic; 
 using System.Linq;  
using System.Text; 
 using System.Threading.Tasks;
 using System.Diagnostics;
namespace Practice {
    class Program
    {
        static void Main(string[] args)
        {
        	
        	
        	Console.Write("Enter the Size of Array \n");//Collects Number of Array
        	int n =Convert.ToInt32(Console.ReadLine()); //Takes the number of array and store it as n
        	int [] mynum = new int[n]; // creates and array of mynum to store the n number of array
        	Console.Write("Enter The Numbers to be Sorted \n");
        	for(int p=0; p<n; p++)// for loop to start from 0 and end at n-1
        	{
        		mynum[p] = Convert.ToInt32(Console.ReadLine());
        	}
        	Console.Write("\n The Unsorted Data \n");
        	Console.WriteLine("");
       foreach (int y in mynum) { // for loop to print the unsorted data
        		Console.Write("\t" + y);
        		
       }
        	for(int i=0;i<n;i++)
        	{
        		for(int j=i+1;j<n;j++)
        		{
        			if(mynum[i]>mynum[j])
        			         {
        			         	int x= mynum[j];
        			         	mynum[j] = mynum[i];
        			         	mynum[i] = x;
        			         }
        			        
        		}
        	
        	
        	}
        	Console.Write("\n The Sorted Array is: \n");
        		Console.WriteLine("");
       foreach (int y in mynum) { // for loop to print the sorted data
        		Console.Write("\t" + y);
        		
       }
        	 
         Console.ReadLine();   
         Console.Write("Press Enter to Continue");
         Console.ReadKey(true);
        
        }
    } }
